import React, {Component} from 'react';
import {Table} from './Table';

export class TicTacToeGame extends Component {

    render() {
        return (
            <div className="tic-tac-toe">
                <header><h1>Крестики-нолики</h1></header>
                <body>
                    <Table/>
                </body>
            </div>
        );
    }
}