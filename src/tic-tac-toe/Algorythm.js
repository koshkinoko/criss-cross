import {circle, cross, goodStartPoints, lines} from './Consts';

export const checkWinner = (lastStepProducer, table) => {
    for(let i=0; i<lines.length; i++) {
        const pointIndexes = lines[i];
        const firstPoint = table[pointIndexes[0][0]][pointIndexes[0][1]] === lastStepProducer;
        const secondPoint = table[pointIndexes[1][0]][pointIndexes[1][1]] === lastStepProducer;
        const thirdPoint = table[pointIndexes[2][0]][pointIndexes[2][1]] === lastStepProducer;
        if(firstPoint && secondPoint && thirdPoint) {
            return pointIndexes;
        }
    }
    return null;
};

export const findBestStep = (symbol, table) => {
    const opposite = symbol === cross ? circle : cross;
    let reservedStep;

    for(let i=0; i<lines.length; i++) {
        const pointIndexes = lines[i];
        let points = [];
        points[0] = table[pointIndexes[0][0]][pointIndexes[0][1]];
        points[1] = table[pointIndexes[1][0]][pointIndexes[1][1]];
        points[2] = table[pointIndexes[2][0]][pointIndexes[2][1]];

        if(!points.join().includes(opposite)) {
            let count = 0;
            const symbolPlaces = points.map((p) => {
                const res = p === symbol ? 1 : 0;
                count += res;
                return res;
            });
            if(count===2) {
                return pointIndexes[symbolPlaces.indexOf(0)];
            }
            if (count === 1 && !reservedStep) {
                reservedStep = pointIndexes[symbolPlaces.indexOf(0)];
            }
        } else if(!points.join().includes(symbol)){
            let count = 0;
            const oppositePlaces = points.map((p) => {
                const res = p === opposite ? 1 : 0;
                count += res;
                return res;
            });
            if(count===2) {
                reservedStep = pointIndexes[oppositePlaces.indexOf(0)];
            }
        }
    }
    if(!reservedStep) {
        reservedStep = getRandomStep(table);
    }
    return reservedStep;
};

const getRandomStep = (table) => {
    let index = Math.floor(Math.random() * goodStartPoints.length);
    let step = goodStartPoints[index];
    if(table[step[0]][step[1]]) {
        step = goodStartPoints[index < goodStartPoints.length - 1 ? index + 1 : index - 1];
    }
    return step;
};