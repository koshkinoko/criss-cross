import React, {Component} from 'react';
import {Ceil} from './Ceil';
import {circle, cross} from './Consts';
import {checkWinner, findBestStep} from "./Algorythm";

export class Table extends Component {

    getInitialState() {
        const initialState = {
            ceils: [["", "", ""], ["", "", ""], ["", "", ""]],
            current: " cross",
            pointIndexes: null,
            stepsLeft: 9
        };
        return initialState;
    }

    constructor(props) {
        super(props);

        let ceils = new Array(3);
        for (let i = 0; i < 3; i++) {
            ceils[i] = ["", "", ""];
        }
        this.state = this.getInitialState();
    }

    handleClick(i, j) {
        let {ceils, current, stepsLeft} = this.state;
        if (!ceils[i][j]) {
            let ceilsNew = [...ceils];
            ceilsNew[i][j] = this.state.current;
            const pointIndexes = checkWinner(current, ceilsNew);
            if (!pointIndexes) {
                current = this.state.current === cross ? circle : cross;
            }
            this.setState({ceils: ceilsNew, current, pointIndexes, stepsLeft: stepsLeft - 1});
        }
    }

    handleHelpClick() {
        const step = findBestStep(this.state.current, this.state.ceils);
        this.handleClick(step[0], step[1]);
    }

    startNewGame() {
        this.setState(this.getInitialState());
    }

    render() {
        const {pointIndexes, stepsLeft} = this.state;
        let trs = [];
        let tds = [];
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                let className = "";
                if (pointIndexes) {
                    className = pointIndexes.find((a) => {
                        return a[0] === i && a[1] === j
                    }) ? " winner" : "";
                }
                tds.push(<td onClick={() => this.handleClick(i, j)}><Ceil
                    content={this.state.ceils[i][j] + className}/></td>);
            }
            trs.push(<tr>{tds}</tr>);
            tds = [];
        }
        return (
            <div>
                <table>
                    <tbody>{trs}</tbody>
                </table>
                {(pointIndexes || !stepsLeft) && < div className="winner-container">
                    {pointIndexes ? <p>Победил игрок {this.state.current === cross ? "X" : "0"}</p> :
                        <p>Ничья!</p>}

                </div>
                }
                <div className="menu-container">
                    {(!pointIndexes && stepsLeft) &&
                    <button className="button" onClick={() => this.handleHelpClick()}>ПОДСКАЗКА</button>}
                    <button className="button" onClick={() => this.startNewGame()}>Начать заново</button>
                    <h1>Текущий ход:</h1>
                    <div className={`ceil${this.state.current}`}>
                    </div>
                </div>
            </div>
        )
    }
}
