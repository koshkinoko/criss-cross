import React, {Component} from 'react';
import './App.css';
import {TicTacToeGame} from './tic-tac-toe/TicTacToeGame';

class App extends Component {
    render() {
        return (
            <div className="App">
                <TicTacToeGame/>
            </div>
        );
    }
}

export default App;
